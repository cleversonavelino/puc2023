import { Component } from '@angular/core';
import { UsuarioModel } from './model/usuario.model';
import { UsuarioService } from './service/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent {

  nomeUsuario: string = '';
  emailUsuario: string = '';
  senhaUsuario: string = '';
  confirmaSenha: string = '';

  constructor(private usuarioService: UsuarioService) { }


salvar() {
  let usuario = new UsuarioModel();
  usuario.nome = this.nomeUsuario;
  usuario.senha = this.senhaUsuario;

  usuario.login = 'loginfake';
  
  this.usuarioService.salvar(usuario).subscribe(usuarioResponse => {
    console.log(usuarioResponse);
  });
}

}
